# soundgame
creates notes for Taiko San Jiro 2 from music.

##preview
watch the movie below:
Japanese: https://qiita.com/woodyOutOfABase/items/01cc43fafe767d3edf62
English: https://medium.com/@shunakamura/automatic-drummer-with-deep-learning-3e92723b5a79

#how to use
download it, and run
```
python3 program/learnconv.py don
python3 program/learnconv.py ka
```
to train, and run
```
python3 program inferconv.py don
python3 program inferconv.py ka
python3 synthesize.py 1
```
to infer.
You may need to preprocess the song with running program/musicprocessor.py.
.tja file will be automatically created, and you can play it in Taiko San Jiro 2. Enjoy it!

# Acknowledgements
The idea to use rhythm game notes as training data set is derived from Dance Dance Convolution,
https://arxiv.org/abs/1703.06891
and the CNN model is from IMPROVED MUSICAL ONSET DETECTION WITH CONVOLUTIONAL NEURAL NETWORKS.
https://ieeexplore.ieee.org/document/6854953/
Thank you very much.

# Warning
Copyright of the music files and .tja files in the data directory doesn't belong to the owner of this repository.
